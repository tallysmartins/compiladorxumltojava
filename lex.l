%{ 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "yacc.tab.h"

%}
 
/* Definições */
OPENCLASS "<Class"
CLOSECLASS "</Class>"
OPENATRIBUTE "<Attribute"
CLOSEATRIBUTE "</Attribute>"
OPENMETHOD "<Operation"
CLOSEMETHOD "</Operation>"
CLOSEOPENTAG ">"
CLOSE "/>"
NAME "Name"
ISABSTRACT "Abstract"
ATTRIBUITION "="
HASGETTER "HasGetter" 
HASSETTER  "HasSetter"
VISIBILITY "Visibility"
OPENTYPE "<Type"
CLOSETYPE "</Type>"
OPENRETURNTYPE "<ReturnType"
CLOSERETURNTYPE "</ReturnType>"
OPENPARAMETER "<Parameter"
CLOSEPARAMETER "</Parameter>"
OPENDATATYPE "<DataType"
CODID [A-Za-z0-9_]+[.][A-Za-z0-9_]+
STRING [A-z][A-z0-9]*
WHITESPACE [ \n\r\t]+
DIGITS [0-9]+
ANY_CHAR .
ID "Id"
ASPAS ["]

 


 
%% /* Regras */
 
{VISIBILITY} 	{return T_VISIBILITY;} 
{OPENTYPE} 		{return T_OPENTYPE;} 

{OPENCLASS} 	{return T_OPENCLASS;}
{CLOSECLASS}  	{return T_CLOSECLASS;}
{OPENATRIBUTE}  {return T_OPENATRIBUTE;}
{CLOSEATRIBUTE} {return T_CLOSEATRIBUTE;}
{OPENMETHOD}  {return T_OPENMETHOD;}
{CLOSEMETHOD} {return T_CLOSEMETHOD;}
{CLOSEOPENTAG}  {return T_CLOSEOPENTAG;}
{CLOSE}  		{return T_CLOSE;}
{ID}            {yylval.strdata = strdup(yytext);  return T_ID;}
{CODID}         {yylval.strdata = strdup(yytext);  return T_CODID;}
{HASGETTER} 	{yylval.strdata = strdup(yytext); return  T_HASGETTER;}
{HASSETTER}		{yylval.strdata = strdup(yytext); return  T_HASSETTER;}
{NAME}  		{yylval.strdata = strdup(yytext); return T_NAME;}
{ISABSTRACT}    {yylval.strdata = strdup(yytext); return T_ISABSTRACT;}
{ATTRIBUITION} 	{yylval.strdata = strdup(yytext); return T_ATTRIBUITION;}
{STRING}  		{yylval.strdata = strdup(yytext);  return  T_STRING;}
{CLOSETYPE} 	{return T_CLOSETYPE;} 
{OPENRETURNTYPE} 		{return T_OPENRETURNTYPE;} 
{CLOSERETURNTYPE} 	{return T_CLOSERETURNTYPE;} 
{OPENPARAMETER} 		{return T_OPENPARAMETER;} 
{CLOSEPARAMETER} 	{return T_CLOSEPARAMETER;} 
{OPENDATATYPE} 	{return T_OPENDATATYPE;} 
{WHITESPACE} 	{}
{DIGITS}  		{}
{ASPAS} 		{}
{ANY_CHAR} 		{printf("Unexpected character in input: '%c' (ASCII=%d)\n", yytext[0], yytext[0]);}
%%